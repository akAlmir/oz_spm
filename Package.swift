// swift-tools-version: 5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "OZLivenessSDK",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "OZLivenessSDK",
            targets: ["OZLivenessSDK", "OZLivenessSDKResources"]
        )
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "OZLivenessSDK",
            path: "./OZLivenessSDK.xcframework"
        ),
        .target(
            name: "OZLivenessSDKResources",
            dependencies: [
                "OZLivenessSDK"
            ],
            resources: [.copy("Resources/OZLivenessSDK.bundle")]
        )
    ]
)