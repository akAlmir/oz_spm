// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9.2 (swiftlang-5.9.2.2.56 clang-1500.1.0.2.5)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name OZLivenessSDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import Accelerate
import CommonCrypto
import Compression
import CoreData
import CoreFoundation
import CoreGraphics
import CoreImage
import CoreText
import Darwin
import DeveloperToolsSupport
import Dispatch
import Foundation
import Foundation/*.Bundle*/
import ImageIO
import MachO
import MobileCoreServices
import ObjectiveC
import QuartzCore
import Swift
import SwiftUI
import SystemConfiguration
import UIKit
import VideoToolbox
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
import simd
@objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) open class AnimatedControl : UIKit.UIControl {
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isEnabled: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isSelected: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isHighlighted: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func beginTracking(_ touch: UIKit.UITouch, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func continueTracking(_ touch: UIKit.UITouch, with event: UIKit.UIEvent?) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func endTracking(_ touch: UIKit.UITouch?, with event: UIKit.UIEvent?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func cancelTracking(with event: UIKit.UIEvent?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var intrinsicContentSize: CoreFoundation.CGSize {
    @objc get
  }
  @_Concurrency.MainActor(unsafe) open func animationDidSet()
  @objc deinit
}
public enum OZError : Foundation.LocalizedError {
  case selfieLengthError
  case expireTokenError
  case environmentSecurityCheckError
  public static func == (a: OZLivenessSDK.OZError, b: OZLivenessSDK.OZError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension OZLivenessSDK.OZError : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension OZLivenessSDK.OZMedia : Swift.Equatable {
  public static func == (left: OZLivenessSDK.OZMedia, right: OZLivenessSDK.OZMedia) -> Swift.Bool
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class LottieView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func didMoveToWindow()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var contentMode: UIKit.UIView.ContentMode {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public struct ResponseError : Foundation.LocalizedError {
  public let code: Swift.Int?
  public let errorType: OZLivenessSDK.ResponseError.ErrorType?
  public var errorDescription: Swift.String? {
    get
  }
  public enum ErrorType {
    case incorrectLoginOrPassword
    case jsonSerializationError
    case wrongURL
    case timeout
    case httpError
    public static func == (a: OZLivenessSDK.ResponseError.ErrorType, b: OZLivenessSDK.ResponseError.ErrorType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
}
public struct Features : Swift.Codable {
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public struct LicenseResponse {
  public let code: OZLivenessSDK.LicenseError
  public let message: Swift.String?
}
public enum LicenseError : Swift.Error {
  case licenseFileNotFound(Swift.String)
  case licenseParseError(Swift.String)
  case licenseBundleError(Swift.String)
  case licenseExpired(Swift.String)
  case licenseSuspended
  case licenseFraudDetected
  case checkLicenseUnknownError
  case invalidLicenseData
  case checkLicenseTemporaryError
  case invalidLicenseProductId
  case licenseRevoked
  case invalidLicenseKey
  case licenseMeterAttributeLimitReached(Swift.String)
  case licenseActivationLimitReached
  case syncCheckLicenseMethodUsed
  case invalidLicenseServerRequest
  case checkLicenseTimeout
  case noConnection
  case masterLicensePublicKeyMissing
  case masterLicensePublicKeyInvalid
  case masterLicenseFraudDetected
  case masterLicenseSignatureMissing
  public var code: Swift.String {
    get
  }
}
extension OZLivenessSDK.LicenseError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
extension UIKit.UIImage {
  convenience public init?(pixelBuffer: CoreVideo.CVPixelBuffer)
}
extension CoreGraphics.CGImage {
  public static func create(pixelBuffer: CoreVideo.CVPixelBuffer) -> CoreGraphics.CGImage?
}
extension UIKit.UIImage {
  public func sha256() -> Swift.String
}
extension Foundation.FileManager {
  public func secureCopyItem(at srcURL: Foundation.URL, to dstURL: Foundation.URL) -> Swift.Bool
}
infix operator +| : DefaultPrecedence
infix operator +- : DefaultPrecedence
public struct Customization : Swift.Codable, Swift.Equatable {
  public var toolbarCustomization: OZLivenessSDK.ToolbarCustomization
  public var antiscamCustomization: OZLivenessSDK.AntiscamCustomization
  public var centerHintCustomization: OZLivenessSDK.CenterHintCustomization
  public var hintAnimationCustomization: OZLivenessSDK.HintAnimationCustomization
  public var faceFrameCustomization: OZLivenessSDK.FaceFrameCustomization
  public var versionCustomization: OZLivenessSDK.VersionLabelCustomization
  public var backgroundCustomization: OZLivenessSDK.BackgroundCustomization
  public var logoCustomization: OZLivenessSDK.LogoCustomization?
  public init(toolbarCustomization: OZLivenessSDK.ToolbarCustomization, antiscamCustomization: OZLivenessSDK.AntiscamCustomization, centerHintCustomization: OZLivenessSDK.CenterHintCustomization, hintAnimationCustomization: OZLivenessSDK.HintAnimationCustomization, faceFrameCustomization: OZLivenessSDK.FaceFrameCustomization, versionCustomization: OZLivenessSDK.VersionLabelCustomization, backgroundCustomization: OZLivenessSDK.BackgroundCustomization, logoCustomization: OZLivenessSDK.LogoCustomization? = nil)
  public static func == (a: OZLivenessSDK.Customization, b: OZLivenessSDK.Customization) -> Swift.Bool
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public struct LogoCustomization : Swift.Codable, Swift.Equatable {
  public var image: UIKit.UIImage
  public var size: CoreFoundation.CGSize
  public init(image: UIKit.UIImage, size: CoreFoundation.CGSize)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.LogoCustomization, b: OZLivenessSDK.LogoCustomization) -> Swift.Bool
}
public struct ToolbarCustomization : Swift.Codable, Swift.Equatable {
  public var closeButtonIcon: UIKit.UIImage?
  public var closeButtonColor: UIKit.UIColor?
  public var titleText: Swift.String?
  public var titleFont: UIKit.UIFont?
  public var titleColor: UIKit.UIColor?
  public var backgroundColor: UIKit.UIColor?
  public init(closeButtonIcon: UIKit.UIImage? = nil, closeButtonColor: UIKit.UIColor? = nil, titleText: Swift.String? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, backgroundColor: UIKit.UIColor? = nil)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.ToolbarCustomization, b: OZLivenessSDK.ToolbarCustomization) -> Swift.Bool
}
public struct AntiscamCustomization : Swift.Codable, Swift.Equatable {
  public var customizationEnableAntiscam: Swift.Bool?
  public var customizationAntiscamTextMessage: Swift.String?
  public var customizationAntiscamTextFont: UIKit.UIFont?
  public var customizationAntiscamTextColor: UIKit.UIColor?
  public var customizationAntiscamBackgroundColor: UIKit.UIColor?
  public var customizationAntiscamCornerRadius: CoreFoundation.CGFloat?
  public var customizationAntiscamFlashColor: UIKit.UIColor?
  public init(customizationEnableAntiscam: Swift.Bool? = nil, customizationAntiscamTextMessage: Swift.String? = nil, customizationAntiscamTextFont: UIKit.UIFont? = nil, customizationAntiscamTextColor: UIKit.UIColor? = nil, customizationAntiscamBackgroundColor: UIKit.UIColor? = nil, customizationAntiscamCornerRadius: CoreFoundation.CGFloat? = nil, customizationAntiscamFlashColor: UIKit.UIColor? = nil)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.AntiscamCustomization, b: OZLivenessSDK.AntiscamCustomization) -> Swift.Bool
}
public struct CenterHintCustomization : Swift.Codable, Swift.Equatable {
  public var textColor: UIKit.UIColor?
  public var textFont: UIKit.UIFont?
  public var verticalPosition: Swift.Int?
  public var backgroundColor: UIKit.UIColor?
  public var hideTextBackground: Swift.Bool?
  public var backgroundCornerRadius: Swift.Int?
  public init(textColor: UIKit.UIColor? = nil, textFont: UIKit.UIFont? = nil, verticalPosition: Swift.Int? = nil, backgroundColor: UIKit.UIColor? = nil, hideTextBackground: Swift.Bool? = nil, backgroundCornerRadius: Swift.Int? = nil)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.CenterHintCustomization, b: OZLivenessSDK.CenterHintCustomization) -> Swift.Bool
}
public struct HintAnimationCustomization : Swift.Codable, Swift.Equatable {
  public var hideAnimation: Swift.Bool
  public var animationIconSize: CoreFoundation.CGFloat
  @available(*, deprecated, renamed: "hintGradientColor")
  public var toFrameGradientColor: UIKit.UIColor?
  public var hintGradientColor: UIKit.UIColor?
  @available(*, deprecated, message: "Use init(hideAnimation: Bool, animationIconSize: CGFloat, hintGradientColor: UIColor) instead")
  public init(hideAnimation: Swift.Bool, animationIconSize: CoreFoundation.CGFloat, toFrameGradientColor: UIKit.UIColor)
  public init(hideAnimation: Swift.Bool, animationIconSize: CoreFoundation.CGFloat, hintGradientColor: UIKit.UIColor)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.HintAnimationCustomization, b: OZLivenessSDK.HintAnimationCustomization) -> Swift.Bool
}
public struct FaceFrameCustomization : Swift.Codable, Swift.Equatable {
  public enum GeometryType : Swift.Codable, Swift.Equatable {
    case oval
    case rectangle(cornerRadius: CoreFoundation.CGFloat)
    case circle
    case square(cornerRadius: CoreFoundation.CGFloat)
    public static func == (a: OZLivenessSDK.FaceFrameCustomization.GeometryType, b: OZLivenessSDK.FaceFrameCustomization.GeometryType) -> Swift.Bool
    public func encode(to encoder: any Swift.Encoder) throws
    public init(from decoder: any Swift.Decoder) throws
  }
  public var strokeWidth: CoreFoundation.CGFloat
  public var strokeFaceAlignedColor: UIKit.UIColor
  public var strokeFaceNotAlignedColor: UIKit.UIColor
  public var geometryType: OZLivenessSDK.FaceFrameCustomization.GeometryType
  @available(*, deprecated)
  public var strokePadding: CoreFoundation.CGFloat
  public init(strokeWidth: CoreFoundation.CGFloat, strokeFaceAlignedColor: UIKit.UIColor, strokeFaceNotAlignedColor: UIKit.UIColor, geometryType: OZLivenessSDK.FaceFrameCustomization.GeometryType, strokePadding: CoreFoundation.CGFloat)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.FaceFrameCustomization, b: OZLivenessSDK.FaceFrameCustomization) -> Swift.Bool
}
public struct VersionLabelCustomization : Swift.Codable, Swift.Equatable {
  public var textFont: UIKit.UIFont?
  public var textColor: UIKit.UIColor?
  public init(textFont: UIKit.UIFont? = nil, textColor: UIKit.UIColor? = nil)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.VersionLabelCustomization, b: OZLivenessSDK.VersionLabelCustomization) -> Swift.Bool
}
public struct BackgroundCustomization : Swift.Codable, Swift.Equatable {
  public var backgroundColor: UIKit.UIColor?
  public init(backgroundColor: UIKit.UIColor? = nil)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
  public static func == (a: OZLivenessSDK.BackgroundCustomization, b: OZLivenessSDK.BackgroundCustomization) -> Swift.Bool
}
public enum AnalysisType : Swift.CustomStringConvertible {
  case quality, document, biometry, blackList
  public static var all: Swift.Set<OZLivenessSDK.AnalysisType> {
    get
  }
  public var description: Swift.String {
    get
  }
  public static func == (a: OZLivenessSDK.AnalysisType, b: OZLivenessSDK.AnalysisType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum SizeReductionStrategy : Swift.String, Swift.CaseIterable {
  case uploadOriginal
  case uploadCompressed
  case uploadBestShot
  case uploadNothing
  public init?(rawValue: Swift.String)
  public typealias AllCases = [OZLivenessSDK.SizeReductionStrategy]
  public typealias RawValue = Swift.String
  public static var allCases: [OZLivenessSDK.SizeReductionStrategy] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public enum AnalysisMode : Swift.Equatable {
  case onDevice, serverBased, hybrid
  public static func == (a: OZLivenessSDK.AnalysisMode, b: OZLivenessSDK.AnalysisMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Analysis {
  public init(media: [OZLivenessSDK.OZMedia]?, type: OZLivenessSDK.AnalysisType, mode: OZLivenessSDK.AnalysisMode, sizeReductionStrategy: OZLivenessSDK.SizeReductionStrategy? = nil, params: [Swift.String : Any]?)
  public init(media: [OZLivenessSDK.OZMedia]?, type: OZLivenessSDK.AnalysisType, mode: OZLivenessSDK.AnalysisMode, sizeReductionStrategy: OZLivenessSDK.SizeReductionStrategy? = nil)
}
public struct AnalysisStatus {
  public let media: OZLivenessSDK.OZMedia
  public let index: Swift.Int
  public let from: Swift.Int
  public let progress: Foundation.Progress
  public let cancelableRequest: any OZLivenessSDK.OZCancelableRequest
}
public enum LicenseSource : Swift.Codable {
  case licenseFilePath(Swift.String)
  case licenseFileName(Swift.String)
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public enum Connection {
  case fromServiceToken(host: Swift.String, token: Swift.String)
  case fromCredentials(host: Swift.String, login: Swift.String, password: Swift.String)
}
public struct LicenseData {
  public var appIDS: [Swift.String]?
  public var expires: Foundation.TimeInterval?
  public var features: OZLivenessSDK.Features?
  public var configs: OZLivenessSDK.ABTestingConfigs?
}
public struct OZFrameCustomization {
  public var backgroundColor: UIKit.UIColor
}
@available(iOS 11.0, *)
public var OZSDK: any OZLivenessSDK.OZSDKProtocol {
  get
}
public func OZSDK(licenseSources: [OZLivenessSDK.LicenseSource], masterLicenseSignature: Swift.String? = nil, completion: @escaping ((OZLivenessSDK.LicenseData?, OZLivenessSDK.LicenseError?) -> Swift.Void))
public func AnalysisRequestBuilder(_ folderId: Swift.String? = nil) -> any OZLivenessSDK.OZRequestProtocol
public typealias FaceCaptureCompletion = (_ results: [OZLivenessSDK.OZMedia]?, _ error: OZLivenessSDK.OZVerificationStatus?) -> Swift.Void
public protocol OZRequestProtocol : AnyObject {
  func addAnalysis(_ analysis: OZLivenessSDK.Analysis)
  func addAnalysis(_ analysis: [OZLivenessSDK.Analysis])
  func uploadMedia(_ media: OZLivenessSDK.OZMedia)
  func uploadMedia(_ media: [OZLivenessSDK.OZMedia])
  func addFolderId(_ folderId: Swift.String)
  func addFolderMeta(_ meta: [Swift.String : Any])
  @discardableResult
  func run(statusHandler: @escaping ((_ status: OZLivenessSDK.RequestStatus) -> Swift.Void), errorHandler: @escaping ((_ error: any Swift.Error) -> Swift.Void), completionHandler: @escaping (_ results: OZLivenessSDK.RequestResult) -> Swift.Void) -> (any OZLivenessSDK.OZCancelableRequest)?
}
public protocol OZSDKProtocol : AnyObject {
  var resultObserver: (([OZLivenessSDK.OZMedia]) -> Swift.Void) { get set }
  var journalObserver: ((Swift.String) -> Swift.Void) { get set }
  var licenseResult: ((OZLivenessSDK.LicenseData?, OZLivenessSDK.LicenseError?) -> Swift.Void) { get set }
  var localizationCode: OZLivenessSDK.OZLocalizationCode? { get set }
  var host: Swift.String { get }
  var eventsHost: Swift.String { get }
  var attemptSettings: OZLivenessSDK.OZAttemptSettings { get set }
  var customization: OZLivenessSDK.Customization { get set }
  var version: Swift.String { get }
  var licenseData: OZLivenessSDK.LicenseData? { get }
  @available(*, deprecated, message: "Use setApiConnection(Connection) method")
  @discardableResult
  func login(_ login: Swift.String, password: Swift.String, completion: @escaping (_ accessToken: Swift.String?, _ error: (any Swift.Error)?) -> Swift.Void) -> any OZLivenessSDK.OZCancelableRequest
  func isLoggedIn() -> Swift.Bool
  func hasEventsConnection() -> Swift.Bool
  func logout()
  @available(*, deprecated, message: "Use setApiConnection(Connection) method")
  func setPermanentAccessToken(_ accessToken: Swift.String)
  func createVerificationVCWithDelegate(_ delegate: any OZLivenessSDK.OZLivenessDelegate, actions: [OZLivenessSDK.OZVerificationMovement]) throws -> UIKit.UIViewController
  func createVerificationVCWithDelegate(_ delegate: any OZLivenessSDK.OZLivenessDelegate, actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], completion: @escaping OZLivenessSDK.FaceCaptureCompletion) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position, completion: @escaping OZLivenessSDK.FaceCaptureCompletion) throws -> UIKit.UIViewController
  func cleanTempDirectory()
  @available(*, deprecated, message: "Use setApiConnection(Connection) method")
  func configure(with host: Swift.String)
  func set(licenseBundle: Foundation.Bundle) throws
  func set(languageBundle: Foundation.Bundle)
  func set(licenseBundle: Foundation.Bundle, licenseSources: [OZLivenessSDK.LicenseSource])
  @available(*, deprecated, message: "Use setLicense(licenseSource: LicenseSource) method")
  func setLicense(from path: Swift.String)
  func setLicense(licenseSource: OZLivenessSDK.LicenseSource)
  func setLicense(licenseSource: OZLivenessSDK.LicenseSource, masterLicenseSignature: Swift.String?)
  func setApiConnection(_ apiConnection: OZLivenessSDK.Connection, completion: @escaping (_ accessToken: Swift.String?, _ error: (any Swift.Error)?) -> Swift.Void)
  func setEventsConnection(_ eventsConnection: OZLivenessSDK.Connection?, completion: @escaping (_ accessToken: Swift.String?, _ error: (any Swift.Error)?) -> Swift.Void)
  func getEventSessionId() -> Swift.String
  func setSelfieLength(length: Swift.Int) throws
}
public protocol OZCancelableRequest {
  func cancel()
}
public struct DocumentPhoto {
  public init(front: Foundation.URL?, back: Foundation.URL?)
  public var front: Foundation.URL?
  public var back: Foundation.URL?
}
public enum OZAnalysesState : Swift.String {
  case liveness
  case quality
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public protocol OZLivenessDelegate : AnyObject {
  func onOZLivenessResult(results: [OZLivenessSDK.OZMedia])
  func onError(status: OZLivenessSDK.OZVerificationStatus?)
}
public struct OZLivenessResult {
  public var status: OZLivenessSDK.OZVerificationStatus {
    get
  }
  public var media: OZLivenessSDK.OZMedia
}
public enum MediaType {
  case movement
  case documentBack
  case documentFront
  public static func == (a: OZLivenessSDK.MediaType, b: OZLivenessSDK.MediaType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct OZMedia {
  public init(movement: OZLivenessSDK.OZVerificationMovement?, mediaType: OZLivenessSDK.MediaType, metaData: [Swift.String : Any]?, additionalTags: [Swift.String]? = nil, videoURL: Foundation.URL?, bestShotURL: Foundation.URL?, preferredMediaURL: Foundation.URL?, timestamp: Foundation.Date)
  public init(mediaType: OZLivenessSDK.MediaType, metaData: [Swift.String : Any]?, additionalTags: [Swift.String]? = nil, preferredMediaURL: Foundation.URL?, timestamp: Foundation.Date)
  public var movement: OZLivenessSDK.OZVerificationMovement?
  public var mediaType: OZLivenessSDK.MediaType
  public var metaData: [Swift.String : Any]?
  public var additionalTags: [Swift.String]?
  public var videoURL: Foundation.URL?
  public var bestShotURL: Foundation.URL?
  public var preferredMediaURL: Foundation.URL?
  public var timestamp: Foundation.Date
  public func validate() -> Swift.Bool
}
public enum OZVerificationStatus : Swift.Equatable {
  public var rawValue: Swift.String {
    get
  }
  public static func == (lhs: OZLivenessSDK.OZVerificationStatus, rhs: OZLivenessSDK.OZVerificationStatus) -> Swift.Bool
  case userNotProcessed
  case failedBecauseUserCancelled
  case failedBecauseCameraPermissionDenied
  case failedBecauseOfBackgroundMode
  case failedBecauseOfTimeout
  case failedBecauseOfAttemptLimit
  case failedBecausePreparingTimout
  case failedBecauseOfLowMemory
  case failedBecauseOfLicenseError(OZLivenessSDK.LicenseError)
}
public struct OZAttemptSettings {
  public var faceAlignmentTimeout: Swift.Double?
  public var uploadMediaSettings: OZLivenessSDK.UploadMediaSettings
  public init(singleCount: Swift.Int? = nil, commonCount: Swift.Int? = nil, faceAlignmentTimeout: Swift.Double? = nil, uploadMediaSettings: OZLivenessSDK.UploadMediaSettings? = nil)
}
public struct UploadMediaSettings {
  public var attemptsCount: Swift.Int
  public var attemptsTimeout: Swift.UInt64
  public init(attemptsCount: Swift.Int, attemptsTimeout: Swift.UInt64)
}
public enum OZVerificationMovement {
  case smile
  case eyes
  case scanning
  case selfie
  case one_shot
  case left
  case right
  case down
  case up
  public static func == (a: OZLivenessSDK.OZVerificationMovement, b: OZLivenessSDK.OZVerificationMovement) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public class AnalyseResolution {
  public var status: OZLivenessSDK.AnalyseResolutionStatus
  public var type: Swift.String
  public var folderID: Swift.String?
  public var score: Swift.Float?
  public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?)
  public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?, score: Swift.Float?)
  @objc deinit
}
public enum AnalysisError : Swift.Error {
  case missingMedia
  case framingFailed
  case analysisTypeNonSupported
  case validateMediaFailed
  case onDeviceDisable
  public var description: Swift.String {
    get
  }
  public static func == (a: OZLivenessSDK.AnalysisError, b: OZLivenessSDK.AnalysisError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public class MediaProgressStatus {
  public init(media: OZLivenessSDK.OZMedia, index: Swift.Int, from: Swift.Int, progress: Foundation.Progress, cancelableRequest: any OZLivenessSDK.OZCancelableRequest)
  final public let media: OZLivenessSDK.OZMedia
  final public let index: Swift.Int
  final public let from: Swift.Int
  final public let progress: Foundation.Progress
  final public let cancelableRequest: any OZLivenessSDK.OZCancelableRequest
  @objc deinit
}
public class RequestStatus {
  public init(status: OZLivenessSDK.ScenarioState, progressStatus: OZLivenessSDK.MediaProgressStatus? = nil)
  public var status: OZLivenessSDK.ScenarioState
  public var progressStatus: OZLivenessSDK.MediaProgressStatus?
  @objc deinit
}
public class RequestResult {
  public var resolution: OZLivenessSDK.AnalyseResolutionStatus
  public var folderID: Swift.String?
  public var analysisResults: [OZLivenessSDK.AnalysisResult]
  public init(resolution: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String? = nil, analysisResults: [OZLivenessSDK.AnalysisResult])
  @objc deinit
}
extension OZLivenessSDK.RequestResult : Swift.CustomDebugStringConvertible {
  public var debugDescription: Swift.String {
    get
  }
}
public class AnalysisResult {
  public init(resolution: OZLivenessSDK.AnalyseResolutionStatus, type: Swift.String, mode: OZLivenessSDK.AnalysisMode, resultsMedia: [OZLivenessSDK.ResultMedia], analysisID: Swift.String? = nil, confidenceScore: Swift.Float? = nil, serverRawResponse: Swift.String? = nil, error: (any Swift.Error)? = nil)
  public var resolution: OZLivenessSDK.AnalyseResolutionStatus
  public var type: Swift.String
  public var mode: OZLivenessSDK.AnalysisMode
  public var analysisID: Swift.String?
  public var error: (any Swift.Error)?
  public var confidenceScore: Swift.Float?
  public var serverRawResponse: Swift.String?
  public var resultsMedia: [OZLivenessSDK.ResultMedia]
  @objc deinit
}
extension OZLivenessSDK.AnalysisResult : Swift.CustomDebugStringConvertible {
  public var debugDescription: Swift.String {
    get
  }
}
public class ResultMedia {
  public init(resolution: OZLivenessSDK.AnalyseResolutionStatus? = nil, media: OZLivenessSDK.OZMedia, sourceID: Swift.String? = nil, isOnDevice: Swift.Bool, confidenceScore: Swift.Float? = nil, mediaType: Swift.String? = nil, error: (any Swift.Error)? = nil)
  public var resolution: OZLivenessSDK.AnalyseResolutionStatus?
  public var sourceID: Swift.String?
  public var isOnDevice: Swift.Bool
  public var confidenceScore: Swift.Float?
  public var mediaType: Swift.String?
  public var media: OZLivenessSDK.OZMedia
  public var error: (any Swift.Error)?
  @objc deinit
}
extension OZLivenessSDK.ResultMedia : Swift.CustomDebugStringConvertible {
  public var debugDescription: Swift.String {
    get
  }
}
@_inheritsConvenienceInitializers public class DocumentAnalyseResolution : OZLivenessSDK.AnalysisResult {
  public func getTextValue(by fieldName: Swift.String) -> Swift.String?
  override public init(resolution: OZLivenessSDK.AnalyseResolutionStatus, type: Swift.String, mode: OZLivenessSDK.AnalysisMode, resultsMedia: [OZLivenessSDK.ResultMedia], analysisID: Swift.String? = nil, confidenceScore: Swift.Float? = nil, serverRawResponse: Swift.String? = nil, error: (any Swift.Error)? = nil)
  @objc deinit
}
public enum AnalyseResolutionStatus : Swift.String {
  case initial
  case processing
  case failed
  case finished
  case declined
  case success
  case operatorRequired
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum ScenarioState {
  case addToFolder, addAnalyses, waitAnalysisResult
  public static func == (a: OZLivenessSDK.ScenarioState, b: OZLivenessSDK.ScenarioState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum OZLocalizationCode {
  case ru, en, hy, kk, ky, tr, es, pt_BR
  case custom(Swift.String)
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
public enum OZJournalEvent : Swift.String {
  case unknown
  case livenessSessionInitialization
  case livenessCheckStart
  case actionStart
  case actionRestart
  case actionFinish
  case actionFailed
  case livenessCheckFinish
  case actionFacePositionFixed
  case actionFacePositionRecommendation
  case actionRecordStart
  case actionRecordFinish
  case actionRecordSaved
  case brightnessChanged
  case error
  case request
  case response
  case analysisRequestRun
  case hybridAnalysisStart
  case onDeviceAnalysisStart
  case onDeviceVideoInfo
  case onDeviceImageInfo
  case onDeviceAlignFace
  case onDeviceModelResult
  case onDeviceError
  case environmentSecurityCheck
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension Foundation.Bundle {
  public static var coreModule: Foundation.Bundle
  public static var onDeviceModule: Foundation.Bundle
}
public struct ABTestingConfigs : Swift.Codable {
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
extension OZLivenessSDK.OZVerificationMovement : Swift.Equatable {}
extension OZLivenessSDK.OZVerificationMovement : Swift.Hashable {}
extension OZLivenessSDK.OZJournalEvent : Swift.Equatable {}
extension OZLivenessSDK.OZJournalEvent : Swift.Hashable {}
extension OZLivenessSDK.OZJournalEvent : Swift.RawRepresentable {}
extension OZLivenessSDK.OZError : Swift.Equatable {}
extension OZLivenessSDK.OZError : Swift.Hashable {}
extension OZLivenessSDK.AnalysisType : Swift.Equatable {}
extension OZLivenessSDK.AnalysisType : Swift.Hashable {}
extension OZLivenessSDK.AnalysisMode : Swift.Hashable {}
extension OZLivenessSDK.ResponseError.ErrorType : Swift.Equatable {}
extension OZLivenessSDK.ResponseError.ErrorType : Swift.Hashable {}
extension OZLivenessSDK.SizeReductionStrategy : Swift.Equatable {}
extension OZLivenessSDK.SizeReductionStrategy : Swift.Hashable {}
extension OZLivenessSDK.SizeReductionStrategy : Swift.RawRepresentable {}
extension OZLivenessSDK.OZAnalysesState : Swift.Equatable {}
extension OZLivenessSDK.OZAnalysesState : Swift.Hashable {}
extension OZLivenessSDK.OZAnalysesState : Swift.RawRepresentable {}
extension OZLivenessSDK.MediaType : Swift.Equatable {}
extension OZLivenessSDK.MediaType : Swift.Hashable {}
extension OZLivenessSDK.AnalysisError : Swift.Equatable {}
extension OZLivenessSDK.AnalysisError : Swift.Hashable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.Equatable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.Hashable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.RawRepresentable {}
extension OZLivenessSDK.ScenarioState : Swift.Equatable {}
extension OZLivenessSDK.ScenarioState : Swift.Hashable {}
